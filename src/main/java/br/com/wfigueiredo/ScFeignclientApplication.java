package br.com.wfigueiredo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ScFeignclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScFeignclientApplication.class, args);
	}
}
