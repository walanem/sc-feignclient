package br.com.wfigueiredo.model;

import lombok.Data;

/**
 * Created by walanem on 06/04/17.
 */
@Data
public class Company {

    private String id;

    private String name;
}
