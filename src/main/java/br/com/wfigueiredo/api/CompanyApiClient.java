package br.com.wfigueiredo.api;

import br.com.wfigueiredo.model.Company;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@FeignClient(value = "${gameserviceconfig.hostname}${gameserviceconfig}")
public interface CompanyApiClient {

    @RequestMapping(value = "/company",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            method = RequestMethod.POST)
    ResponseEntity<Company> create(@RequestBody Company company);

    @RequestMapping(value = "/company",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            method = RequestMethod.GET)
    ResponseEntity<List<Company>> findAll();

    @RequestMapping(value = "/company/{companyId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            method = RequestMethod.GET)
    ResponseEntity<Company> findById(@PathVariable("companyId") String companyId);

    @RequestMapping(value = "/company/{companyId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> delete(@PathVariable("companyId") String companyId);

    @RequestMapping(value = "/company",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteAll();
}
